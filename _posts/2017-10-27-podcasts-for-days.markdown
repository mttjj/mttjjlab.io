---
title: Podcasts For Days
---
Podcasts are what get me through commutes, workouts, and household chores. Here are the shows I'm currently subscribed to. All links are to the podcast's website unless noted otherwise.

[*99% Invisible*](http://99pi.org) - a really neat show about the design of things we may encounter every day but rarely think about.

[*Accused*](https://www.cincinnati.com/series/accused/) - currently on its second season. Cincinnati Enquirer journalist investigates an unsolved murder that happened 40 years ago.

[*Crimetown*](http://www.crimetownshow.com) - currently in between its first and second season. The first season focused on corruption and organized crime in Providence, Rhode Island.

[*Criminal*](http://thisiscriminal.com) - a true crime podcast that analyses crime from the 'sociological, historical, and anthropological' perspective.

[*DIS-Order: Every Disney Film* (iTunes link)](https://itunes.apple.com/us/podcast/dis-order-every-disney-film/id1238866553?mt=2) - relatively new podcast that discusses each Disney animated feature film in depth, in order. Very fun show.

[*Embedded*](http://www.npr.org/podcasts/510311/embedded) - the episodes take a deep dive into a particular news story or event. The current season, however, is taking a close look at the personal and public histories of Trump and his closest advisors in the current administration.

[*GoNintendo Podcast*](https://gonintendo.com/podcast) - one of the first podcasts I ever subscribed to. They have published a new show every. single. week for over 12 years now. It's incredible. It's just a lot of silly fun with a bit of Nintendo news thrown in.

[*Homecoming*](https://gimletmedia.com/homecoming/) - a scripted psychological thriller. Just finished its second season.

[*Invisibilia*](http://www.npr.org/podcasts/510307/invisibilia) - a show 'about the invisible forces that control human behavior'. Currently between its second and third season.

[*LeVar Burton Reads*](http://www.levarburtonpodcast.com) - recently found this podcast. LeVar finds short fiction that he loves and reads it to you. Simple but delightful.

[*Liar City*](http://liarcity.com) - infrequent podcast that dives deep into discussion about some of history's greatest liars.

[*Mac OS Ken*](http://macosken.com) - daily Apple news show.

[*Mac Cast*](http://www.maccast.com) - another one of the first podcasts I ever subscribed to. Adam gives news updates for the first half of the show and then dives into tips, tricks, feedback and more for the second half. He also occasionally interviews guests about various technology topics.

[*MuggleCast*](http://mugglecast.com) - a classic. Keeps me up to date on my HP news and then has some really in-depth discussions about the wizarding world. The show went on hiatus a few years back but has seen a strong resurgence with the release of the _Fantastic Beasts_ films.

[*Pixar Post Podcast*](http://www.pixarpost.com/p/podcast.html) - fun show all about the world of Pixar. TJ and Julie discuss news relevant to Pixar but also often have some really awesome interviews with Pixar employees.

[*Planet Money*](http://www.npr.org/podcasts/510289/planet-money/) - not as boring as it sounds! Actually, this is one of my favourite podcasts. It discusses economics but with some very interesting stories.

[*Serial*](https://serialpodcast.org) - yes, Serial. I became completely enthralled with Adnan's story in season one. Season two was not quite as interesting for me but I'm still looking forward to season three.

[*Taking the Bullet* (iTunes link)](https://itunes.apple.com/ie/podcast/taking-the-bullet-an-angry-podcast-about-bad-movies/id975915471?mt=2) - very infrequent show where the hosts discuss terrible films they have seen. I have only listened to a few episodes because they are published so infrequently. It remains to be seen whether or not I'll continue to subscribe.

[*Twenty Thousand Hertz*](https://www.20k.org) - interesting show about the way sound affects our entire lives. Somewhat like "99% Invisible"" but dealing with audio.

[*Ungeniused*](https://www.relay.fm/ungeniused) - each episode is short and sweet discussing a weird or strange Wikipedia article.

[*Up First*](http://www.npr.org/podcasts/510318/up-first) - recently subscribed to this one. Daily. Gives a great brief rundown of what the world will be paying attention to that day.

[*Wait Wait...Don't Tell Me!*](http://www.npr.org/programs/wait-wait-dont-tell-me/) - been a longtime subscriber to this one. Always a hilarious show.

[*What Trump Can Teach Us About Con Law*](https://www.radiotopia.fm/podcasts/trump-con-law) - fascinating show. The Trump presidency has brought with it a whole new perspective on how we as a country read and interpret our Constitution. The show discusses Trump's tweets, statements, and more.