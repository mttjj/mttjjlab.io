---
title: You Should Not Force Quit Apps on iOS
---

> The single biggest misconception about iOS is that it’s good digital hygiene to force quit apps that you aren’t using. The idea is that apps in the background are locking up unnecessary RAM and consuming unnecessary CPU cycles, thus hurting performance and wasting battery life.

John does a fantastic job of breaking down why this entire mentality is wrong. I see people do this all the time and it drives me crazy. I'd say something to them but I know they probably wouldn't believe me anyway. Well, now I've got the [post](https://daringfireball.net/2017/07/you_should_not_force_quit_apps) bookmarked so it'll be easy to pull up and say 'See! I told you so!'.