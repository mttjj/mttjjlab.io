---
title: A Goodbye and a Hello
---
It’s currently 11.15pm on December 31 as I write this and I don’t think I’m going to stay up until midnight. Not really my thing. But I did want to write this and post it before 2020. The last post I wrote on this site was almost exactly two years ago. I let 2018 pass without writing anything and I couldn’t let 2019 do the same. 

I have a few ideas for some series I want to blog about over the coming year. I won’t promise anything but I really would like this place to be more active. 

Goodbye 2019. Hello 2020.