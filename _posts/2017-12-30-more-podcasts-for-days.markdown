---
title: (More) Podcasts For Days
---
In the two months since I wrote my last post I've found a few more podcasts to listen to. Again, all links are to the podcast's website unless otherwise noted.

As a follow up to a comment I made on the last post: I wasn't sure how long I was going to remain subscribed to 'Taking the Bullet'. It wasn't very long. I had to unsubscribe. The audio quality of the show was just terrible and really not worth listening to. But, as outlined below, I found two other 'bad movie' podcasts to take its place.

[*Heaven's Gate*](https://www.heavensgate.show) - a fascinating 10-episode mini-series about the Heaven's Gate cult and the people behind it. I made short work of listening to this podcast.

[*Hidden Brain*](https://www.npr.org/podcasts/510308/hidden-brain) - another great NPR production. This show 'reveals the unconscious patterns that drive human behavior, the biases that shape our choices, and the triggers that direct the course of our relationships.'

[*The Indicator from Planet Money*](https://www.npr.org/podcasts/510325/the-indicator-from-planet-money) - a Planet Money spin-off show. Short and sweet about three times per week discussing some sort of numerical 'indicator' pertaining to the global economy.

[*Nintendo Power Podcast*](https://soundcloud.com/nintendopowerpodcast) - Nintendo Power is back! And it's a ... podcast? Yes! There's only been one episode as of this writing but I'm so anxious to keep listening.

[*Small Town Dicks*](http://www.smalltowndicks.com) - a true-crime detective show where identical twin detectives Dan and Dave discuss cases that they've been involved in. I was hooked from the very first episode.

[*Stinker Madness - The Bad Movie Podcast*](http://www.stinkermadness.com) - a podcast about bad movies.

[*How Did This Get Made?*](http://www.earwolf.com/show/how-did-this-get-made/) - another podcast about bad movies.