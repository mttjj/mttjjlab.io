---
title: The Obligatory 'Hello, World'
---

Yes, we're trying something new here. I've been wanting to do this for a very long time and so I finally did it. Gone are the days of a tumblr blog masquerading as a stand-along blog. This is the real deal. This is a completely static website made all on my own. The engine powering the blog is [Jekyll](http://jekyllrb.com). I've spent quite a while trying to get everything up and running. This is still a work-in-progress so there will probably be some bumps along the way. I hope to invest some more time into making the place look a little better. But for now enjoy the show.