---
title: On DRY and the Cost of Wrongful Abstractions
---

> But I also urge you to not follow [software principles] blindly. Put everything you learned in context and always question the validity of your ideas and actions. This is the only sane way towards becoming a better professional.

This quote nicely sums up why writing software can sometimes be quite a chore. I  find that we frequently try to shoehorn our unique problem into a solution defined by - and only by - one of these principles. Do this for the wrong type of situation and, at worst it leads to code that is poorly written and riddled with defects. Even at best, the code is probably hard to understand.

Software patterns, designs, and principles are all great starting points but don't let them limit your solutions to the problems you are trying to solve. The solution needs to be the right one; not the one that lives completely within the confines of a 'rule' you read in some book. Sometimes those two things are the same. Often, they are not. Don't let that concern you.

Be sure to read the [original article](http://thereignn.ghost.io/on-dry-and-the-cost-of-wrongful-abstractions/) for more discussion.