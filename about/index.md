---
title: about
---

So this is my website. I'm not sure how you stumbled upon it but thank you for visiting. Posts here will likely be infrequent and cover a wide range of topics. All content that is not mine is © of its respective owners.

I'm a software engineer for Cerner Corporation in Kansas City, Missouri. I've been there since I graduated from Kansas State University in 2013. I enjoy writing software, reading, playing video games, and binge watching TV shows and movies. Feel free to check out what I'm doing on any of my social links at the bottom of this page.
